#ifndef JoystickDef
#define JoystickDef

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

class Joystick {
private:
	byte xAxisPin;
	byte yAxisPin;
	byte buttonPin;

	int xAxis;
	int yAxis;
	int xCalibration;
	int yCalibration;

	bool button;
public:
	Joystick(byte xAxisPin, byte yAxisPin, byte buttonPin);

	// Return TRUE if state is updated
	bool update();

	bool isClicked();
	int getXAxis();
	int getYAxis();
};

#endif // !JoystickdEF


