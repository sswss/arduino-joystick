/*
 Name:		TestJoystick.ino
 Created:	2/20/2018 11:38:45 PM
 Author:	sSwSs
*/

#include "Joystick.h"

Joystick * joystick;

// the setup function runs once when you press reset or power the board
void setup() {
	joystick = new Joystick(A3, A2, 4);
	Serial.begin(9600);
}

// the loop function runs over and over again until power down or reset
void loop() {
	delay(100);

	bool isChanged = joystick->update();

	if (isChanged) {
		Serial.print("X: ");
		Serial.println(joystick->getXAxis());

		Serial.print("Y: ");
		Serial.println(joystick->getYAxis());

		Serial.print("CLICKED: ");
		Serial.println(joystick->isClicked() ? "YES" : "NO");
	}
}
