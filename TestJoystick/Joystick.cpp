#include "Joystick.h"

Joystick::Joystick(byte xAxisPin, byte yAxisPin, byte buttonPin): xAxisPin(xAxisPin), yAxisPin(yAxisPin), buttonPin(buttonPin) {
	xAxis = 0;
	yAxis = 0;
	button = false;
	xCalibration = 0;
	yCalibration = 0;

	//activate pull-up resistor on the push-button pin
	pinMode(buttonPin, INPUT_PULLUP);

	pinMode(xAxisPin, INPUT);
	pinMode(yAxisPin, INPUT);

	update();
}

bool Joystick::update() {
	int xPosition = (512 - analogRead(xAxisPin)) * -1;
	int yPosition = 512 - analogRead(yAxisPin);
	int buttonState = digitalRead(buttonPin) == 0;

	// Calibration
	if (xCalibration == 0 && yCalibration == 0) {
		xCalibration = xPosition;
		yCalibration = yPosition;
	}

	xPosition = (float)(xPosition - xCalibration) / 10;
	yPosition = (float)(yPosition - yCalibration) / 10;

	bool isChanged = xPosition != xAxis || yPosition != yAxis || buttonState != button;

	xAxis = xPosition;
	yAxis = yPosition;
	button = buttonState;

	return isChanged;
}

bool Joystick::isClicked() {
	return button;
}

int Joystick::getXAxis() {
	return xAxis;
}

int Joystick::getYAxis() {
	return yAxis;
}
